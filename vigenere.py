class Vigenere:
    def __init__(self, plaintext, key):
        self.plaintext = plaintext
        self.key = key
        self.ignore = ' !@#$%^&*()_+-=`~[]{};\':"<>?,./\\|'
        self.alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    def grid(self):
        grid = {}

        alphabet = list(self.alphabet)
        for l1 in alphabet:
            grid[l1] = {}

            for l2 in alphabet:
                grid[l1][l2] = ''

        for i in range(len(alphabet)):
            count = 0
            letter = alphabet[i]

            for x in range(i, len(alphabet) - 1):
                grid[letter][alphabet[count]] = alphabet[x]
                count+=1

            temp = range(i)
            for index in temp:
                grid[letter][alphabet[count]] = alphabet[index]
                count+=1

        return grid

    def encipher(self):
        ciphertext = []
        ignore = list(self.ignore)
        plain_temp = list(self.plaintext)
        key_temp = list(self.key)
        offset = 0
        grid = self.grid()

        for i in range(len(plain_temp)):
            if i > (len(key_temp) - 1):
                key_temp.append(key_temp[offset])
                offset+=1

                if (offset == len(key_temp)):
                    offset = 0
        print(plain_temp)
        print(key_temp)
        for i in range(len(plain_temp)):
            l1 = plain_temp[i]
            l2 = key_temp[i]

            if l1 in ignore:
                ciphertext.append(l1)
            else:
                ciphertext.append(grid[l1][l2])

        return ''.join(ciphertext)

    def decipher(self):
        None
