class Vigenere {
    constructor(plaintext, key) {
        this.plaintext = plaintext.toUpperCase()
        this.key = key.toUpperCase()
        this.ignore = '!@#$%^&*()_+-=`~[]{};\':"<>?,./\\|'
        this.alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    }

    grid = () => {
        let grid = {}
        
        const alphabet = this.alphabet.split('')
        alphabet.forEach(l1 => {
            grid[l1] = {}
            alphabet.forEach(l2 => {
                grid[l1][l2] = ''
            })
        })

        alphabet.forEach((letter, i) => {
            let count = 0
            for (let x = i; x < alphabet.length; x++) {
                grid[letter][alphabet[count]] = alphabet[x]
                count++
            }

            let temp = [...Array(i).keys()]
            temp.forEach(index => {
                grid[letter][alphabet[count]] = alphabet[index]
                count++
            })
        })

        return grid
    }

    encipher = () => {
        let ciphertext = []
        let ignore = this.ignore.split('')
        let plain_temp = this.plaintext.split('')
        let key_temp = this.key.split('')
        let offset = 0
        let grid = this.grid()

        for (let i in plain_temp) {
            if (!key_temp[i]) {
                key_temp.push(key_temp[offset])
                offset++

                if (offset === key_temp.length)
                    offset = 0
            }
        }

        plain_temp.forEach((l1, index) => {
            let l2 = key_temp[index]
            if (ignore.find(x => x === l1) != null) {
                ciphertext.push(l1)
            }
            else {
                ciphertext.push(grid[l1][l2])
            }
        })

        return ciphertext.join('')
    }

    decipher = () => {}
}

let cp = new Cipher('THE ENEMY', "ABRA")
cp.encipher()